import React from 'react';

export default function Home() {
    return (
        <main id="content" role="main">
            <div className="section">
                <div className="container">
                    <header className="entry-header">
                        <h2 className="entry-title">
                            <p>Seja bem vindo ao LookingBy, seu serviço de monitoramento de posts.</p>
                        </h2>
                    </header>
                </div>
            </div>
        </main>
    );
}