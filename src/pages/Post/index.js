import React, { Component } from 'react';
import Alert from 'react-bootstrap/Alert';

import CompanyPage from '../Company';

import List from '../../components/List';

import gql from 'graphql-tag';
import { Query } from 'react-apollo';

export const GET_USER_POSTS = gql`
query userPosts($filtro: UserFiltro!){
    userPosts(
        filtro:$filtro
      ){
        name
        company{
          name
        }
        posts{
            id
          title
          body
        }
      }
  }
`;

class Post extends Component {
    _getQueryVariables = () => {
        const data = { id: parseInt(this.props.match.params.id) };
        return { filtro: data }
    }

    _getPostsToRender = data => {
        const posts = data.userPosts;
        return posts
    }

    render() {
        return (
            <main id="content" role="main">
                <div className="section">
                    <div className="container">
                        <div className="row">
                            <CompanyPage />
                            <Query query={GET_USER_POSTS} variables={this._getQueryVariables()}>
                                {({ loading, error, data }) => {
                                    if (loading) return (
                                        <div className="three-quarters-block">
                                            Carregando...
                                        </div>
                                    )
                                    if (error) {
                                        return (<Alert variant="danger">
                                            <Alert.Heading>Opa! Ocorreu um erro na execução da requisição!</Alert.Heading>
                                            <p>
                                                Realize novamente a consulta ou atualize a página.
                                                </p>
                                        </Alert>
                                        )
                                    }
                                    const postsToRender = this._getPostsToRender(data)

                                    return (
                                        <List listPosts={postsToRender} />
                                    )
                                }
                                }
                            </Query>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default Post;