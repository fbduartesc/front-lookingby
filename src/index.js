import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import App from './App';

const cache = new InMemoryCache();

const API_BASE_URL = process.env.REACT_APP_BASE_URL;

const customFetch = (uri, options) => {
  return fetch(uri, options)
  .then(response => {
    if (response.status >= 500) {
      return Promise.reject(response.status);
    }
    return response;
  });
};

const httpLink = new HttpLink({
  uri: API_BASE_URL,
  fetch: customFetch
});

const client = new ApolloClient({
  cache,
  link: httpLink
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
);