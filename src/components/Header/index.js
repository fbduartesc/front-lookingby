import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () => (
  <div id="masthead">
    <div id="site-header" role="banner">
      <div className="container">
        <div className="row">
          <Navbar expand="lg"  variant="dark" id="main-menu">
            <Navbar.Brand href="/">LookingBy</Navbar.Brand>
            <Nav className="mr-auto horizontal-navigation">
              <Nav.Link href="/">Início</Nav.Link>
              <Nav.Link href="/posts/1">Posts</Nav.Link>              
            </Nav>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          </Navbar>
        </div>
        <div className="row">
          <h2 className="entry-title">Monitoramento de posts para análise</h2>
          <p className="description"></p>
        </div>
      </div>
    </div>
    
  </div>
);

export default Header;