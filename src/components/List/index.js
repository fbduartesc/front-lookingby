import React, { Component } from 'react';

function ListPosts(props) {
    return (
        <div className="three-quarters-block">
            <div className="content">
                {props.posts.posts.map(post => (
                    <article key={post.id}
                        className="post summary hentry">
                        <header className="entry-header">
                            <h2 className="entry-title">{post.title}</h2>
                            <div className="entry-meta">
                                <span className="post-author">
                                    <i className="fa fa-user fa-fw"></i> Funcionário: <span className="vcard">{props.posts.name}</span>
                                </span>
                                <span className="post-categories">
                                    <i className="fa fa-folder fa-fw"></i> Empresa: {props.posts.company.name}
                                </span>
                            </div>
                        </header>
                        <div className="entry-summary">
                            <p>{post.body}</p>
                        </div>
                    </article>
                ))}
            </div>
        </div>
    );
}

class List extends Component {

    render() {
        const toRender = this.props.listPosts;
        return (
            <ListPosts posts={toRender} />
        )
    }
}

export default List;