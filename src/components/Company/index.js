import React from 'react';
import { Link } from 'react-router-dom';
import Alert from 'react-bootstrap/Alert';

import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const GET_COMPANIES = gql`
  {
    company{        
        id
        name
    }
  }
`;

export default function Company() {
    return (
        <div className="one-quarter-block" role="complementary">
            <div className="sidebar">
                <div className="widget">
                    <h3 className="widget-title">Empresas</h3>
                    <Query query={GET_COMPANIES}>
                        {({ loading, error, data }) => {
                            if (loading) return <div>Carregando...</div>
                            if (error) {
                                return <Alert variant="danger">
                                    <Alert.Heading>Opa! Ocorreu um erro na execução da requisição!</Alert.Heading>
                                    <p>Realize novamente a consulta ou atualize a página.</p>
                                </Alert>
                            }

                            return (
                                <ul>
                                    {data.company.map((company, index) => (
                                        <li key={index}>
                                            <Link className="link"
                                                title={company.name}
                                                to={`/posts/${company.id}`}>{company.name}
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            )
                        }
                        }
                    </Query>
                </div>
            </div>
        </div>
    );
}