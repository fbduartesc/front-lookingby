const express = require('express');
const path = require('path');
const cors = require('cors');
const serveStatic = require('serve-static');

const app = express();
app.use(cors());
app.use(express.json());
app.use(serveStatic(path.join(__dirname, 'build')));

const port = process.env.PORT || 3000;
app.listen(port);
console.log('server started ' + port);